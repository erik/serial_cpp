// Use of libserialport based on https://gist.github.com/Nixes/78e401234e66aa131547d7b78135271c

#include <iostream>
#include <libserialport.h>

void list_ports() {
    int32_t i;
    struct sp_port **ports;

    sp_return error = sp_list_ports(&ports);
    if (error == SP_OK) {
        for (i = 0; ports[i]; i++) {
            std::cout << "Found port: " << sp_get_port_name(ports[i]) << '\n';
        }
        sp_free_port_list(ports);
    } else {
        std::cout << "No serial devices detected\n";
    }
}

int main() {
    std::cout << "Searching for serial ports.\n";
    list_ports();

    //const char* desired_port = "/dev/cu.usbserial-AC01YB5P";
    //const char* desired_port = "/dev/ttyUSB0";
    const char* desired_port = "/dev/ttyACM0";
    constexpr uint32_t baud_rate = 115200;
    constexpr uint32_t byte_buffer_size = 512;
    char byte_buffer[byte_buffer_size];

    struct sp_port *port;
    std::cout << "Opening port " << desired_port << '\n';
    sp_return error = sp_get_port_by_name(desired_port, &port);
    if (error == SP_OK) {
        error = sp_open(port, SP_MODE_READ);
        if (error == SP_OK) {
            sp_set_baudrate(port, baud_rate);
            while (true) {
                int bytes_waiting = sp_input_waiting(port);
                if (bytes_waiting > 0) {
                    std::cout << bytes_waiting << " bytes in the buffer: ";
                    int byte_num = 0;
                    byte_num = sp_nonblocking_read(port, byte_buffer, byte_buffer_size);
                    //bool button_pressed = false;
                    for (decltype(byte_num) i = 0; i < byte_num; ++i){
                        //if (byte_buffer[i] == '1') {
                        //    button_pressed = true;
                        //}
                        std::cout << byte_buffer[i];
                    }
                    std::cout << '\n';
                    /*
                    if (button_pressed) {
                        std::cout << "   the button is pressed!";
                    }
                    std::cout << std::endl;
                    */
                }
            }
            // Execution can never make it here, but I'll close the port on principle.
            sp_close(port);
        } else {
            std::cout << "Error opening serial device\n";
            switch(error) {
                case SP_ERR_ARG:
                    std::cout << "Bad arguments\n";
                    break;
                case SP_ERR_FAIL:
                    std::cout << "OS reported failure\n";
                    std::cout << sp_last_error_code() << '\n';
                    std::cout << sp_last_error_message() << '\n';
                    break;
                case SP_ERR_SUPP:
                    std::cout << "Not supported\n";
                    break;
                case SP_ERR_MEM:
                    std::cout << "OOM\n";
                    break;
                default:
                    std::cout << "No idea\n";
            }
        }
    } else {
        std::cout << "Error finding serial device\n";
    }

    return 0;
}

